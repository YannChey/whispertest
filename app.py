from flask import Flask, jsonify, request
from celery import Celery
import whisper

app = Flask(__name__)
app.config['CELERY_BROKER_URL'] = 'redis://localhost:6379/0'
app.config['CELERY_RESULT_BACKEND'] = 'redis://localhost:6379/0'

celery = Celery(app.name, broker=app.config['CELERY_BROKER_URL'])
celery.conf.update(app.config)


@celery.task
def process_task(audio_data):
    # Code pour transformer l'audio en texte

    model = whisper.load_model("base")
    result = model.transcribe(audio_data)
    print(result["text"])    # Retournez le texte résultant
    return result["text"]


@app.route('/transformation-audio', methods=['POST'])
def transformation_audio():
    audio_data = request.get_data()  # Récupère les données audio depuis la requête
    task = process_task.delay(audio_data)  # Démarre la tâche en arrière-plan

    # Vous pouvez retourner une réponse JSON indiquant l'ID de la tâche
    return jsonify({'task_id': task.id})


@app.route('/transformation-audio/<task_id>', methods=['GET'])
def get_transformation_result(task_id):
    task = process_task.AsyncResult(task_id)

    if task.state == 'PENDING':
        response = {
            'state': task.state,
            'message': 'En attente'
        }
    elif task.state != 'FAILURE':
        response = {
            'state': task.state,
            'text': task.result
        }
    else:
        # La tâche a échoué
        response = {
            'state': task.state,
            'message': str(task.info)
        }

    return jsonify(response)


if __name__ == '__main__':
    app.run(debug=True)